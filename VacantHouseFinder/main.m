//
//  main.m
//  VacantHouseFinder
//
//  Created by ActivateInnovation on 8/1/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
