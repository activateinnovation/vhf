//
//  ViewController.h
//  VacantHouseFinder
//
//  Created by ActivateInnovation on 8/1/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface ViewController : UIViewController <UITextViewDelegate, UIImagePickerControllerDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *houseImageView;

- (IBAction)takePicture:(id)sender;
- (IBAction)sendEmail:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *streetAddress;
@property (strong, nonatomic) IBOutlet UITextField *city;
@property (strong, nonatomic) IBOutlet UITextField *state;
@property (strong, nonatomic) IBOutlet UITextField *zipCode;
@property (strong, nonatomic) IBOutlet UITextView *informationTextView;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)keyboardNext:(id)sender;
@property (strong, nonatomic) UIImage *selectedImage;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

//@property (strong, nonatomic) MFMailComposeViewController *emailDialog;

@end
