//
//  ViewController.m
//  VacantHouseFinder
//
//  Created by ActivateInnovation on 8/1/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize houseImageView;
@synthesize streetAddress, state, city, zipCode, informationTextView;
@synthesize selectedImage;
@synthesize imageView;
//@synthesize emailDialog;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self.informationTextView.layer setCornerRadius:6.5f];
    [self.informationTextView.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    [self.informationTextView.layer setBorderWidth:1.2f];
    self.informationTextView.delegate = self;
    
   
    // [self.imageView.layer setBorderColor:[UIColor blackColor].CGColor];
   // [self.imageView.layer setBorderWidth:2.0];
    
    UIImageView *background = [[UIImageView alloc] initWithFrame: self.view.frame];
    [background setImage:[UIImage imageNamed:@"background1.jpg"]];
   // background.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"p6_@2x"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)takePicture:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}


- (IBAction)sendEmail:(id)sender {
    
    if(self.streetAddress.text.length < 1 || self.city.text.length < 1 || self.state.text.length < 1 || self.zipCode.text.length < 1 || [self.imageView.image isEqual:[UIImage imageNamed:@"home-grey.png"]]){  //
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"There is missing information from the form. Please fill in the blank fields and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else{  //ALL FIELDS ARE FILLED, SEND EMAIL
        
        
        MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
        emailDialog.mailComposeDelegate = self;
        
        NSData *jpegData = UIImageJPEGRepresentation(selectedImage, 1.0f);
        
        NSString *fileName = @"test";
        fileName = [fileName stringByAppendingPathExtension:@"jpeg"];
        [emailDialog addAttachmentData:jpegData mimeType:@"image/jpeg" fileName:fileName];
        NSArray *recipients = [[NSArray alloc] initWithObjects:@"bensouchek@gmail.com", nil];
        [emailDialog setToRecipients:recipients];
        [emailDialog setSubject:@"New Vacant House Listing"];
        
        NSString *comments = @"N/A";
        if(self.informationTextView.text.length > 1){
            
            comments = self.informationTextView.text;
            
        }
        [emailDialog setMessageBody:[NSString stringWithFormat:@"A new house listing has been submitted from the mobile application, its information is listed below along with an attached picture of the property.\n\nStreet: %@\nCity: %@\nState: %@\nZipCode: %@\n\nAdditional Comments: %@", self.streetAddress.text, self.city.text, self.state.text, self.zipCode.text, comments] isHTML:NO];
        
        
      //  [self presentViewController:emailDialog animated:YES completion:nil];
        
        if([MFMailComposeViewController canSendMail] == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Your email is not properly configured on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else{
            
            [self presentViewController:emailDialog animated:YES completion:nil];
        }
        
        
    }
    
    
    
}




- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
 
//NSLog(@"HEREOUTSIDE");
    
    
    [self dismissViewControllerAnimated:YES completion: ^{
        
           // NSLog(@"HEREINSIDE");
       // [self.view setTransform:CGAffineTransformMakeRotation(-1.57)];
       // [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
        
        if(result == MFMailComposeResultSent){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information Sent" message:@"The vacant house information was sucessfully sent. Thank you." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            self.streetAddress.text = @"";
            self.city.text = @"";
            self.state.text = @"";
            self.zipCode.text = @"";
            self.informationTextView.text = @"";
            self.imageView.image = [UIImage imageNamed:@"home-grey.png"];
            
            
        }
        else if(result == MFMailComposeResultFailed)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"This information failed to send. Check your email settings and internet connection and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            
            
        }
        
        
        
    }];
    
}

/*-(BOOL) isNetworkAvailable {
    
UIAlertView *errorView;
    BOOL answer;

if([[Reachability sharedReachability] internetConnectionStatus] == NotReachable) {
    answer = NO;
    errorView = [[UIAlertView alloc]
                 initWithTitle: @"Network Error"
                 message: @"The internets are down, everyone run for your lives!"
                 delegate: self
                 cancelButtonTitle: @"Nooooooo!" otherButtonTitles: nil];
}
else
{
    answer = YES;
    errorView = [[UIAlertView alloc]
                 initWithTitle: @"Whew!"
                 message: @"Relax, the internets are here."
                 delegate: self
                 cancelButtonTitle: @"Yay!" otherButtonTitles: nil];
}

[errorView show];
    
    return answer;

}*/


- (IBAction)dismissKeyboard:(id)sender {
    [self.streetAddress resignFirstResponder];
    [self.city resignFirstResponder];
    [self.state resignFirstResponder];
    [self.zipCode resignFirstResponder];
    [self.informationTextView resignFirstResponder];
    
}

- (IBAction)keyboardNext:(id)sender {
    
    if(sender == streetAddress){
        
        [self.streetAddress resignFirstResponder];
        [self.city becomeFirstResponder];
        
    }
    else if(sender == self.city){
        
        [self.city resignFirstResponder];
        [self.state becomeFirstResponder];
        
    }
    else if(sender == self.state){
        
        [self.state resignFirstResponder];
        [self.zipCode becomeFirstResponder];
        
        
    }else{
        
        [self.zipCode resignFirstResponder];
        [self.informationTextView becomeFirstResponder];
        
        
    }
    
}


- (BOOL) textView: (UITextView*) textView
shouldChangeTextInRange: (NSRange) range
  replacementText: (NSString*) text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.selectedImage = chosenImage;
    

        
        //[self.imageView.layer setBorderColor:[UIColor clearColor].CGColor];
       // [self.imageView.layer setBorderWidth:0.0];
      //  [self.imageView setBackgroundColor:[UIColor clearColor]];
    
        self.imageView.image = self.selectedImage;
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //[self.imageView.layer setBorderColor:[UIColor blackColor].CGColor];
    //[self.imageView.layer setBorderWidth:2.0f];
   // [self.imageView setBackgroundColor:[UIColor lightGrayColor]];
    
    self.imageView.image = self.selectedImage;
    
}




@end
