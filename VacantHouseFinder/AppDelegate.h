//
//  AppDelegate.h
//  VacantHouseFinder
//
//  Created by ActivateInnovation on 8/1/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
